﻿using System.Web.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net;
using System.IO;
using Nop.Core;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Localization;
using Nop.Core.Domain.Vendors;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Tax;
using Nop.Core.Domain.Messages;
using Nop.Core.Domain.Security;
using Nop.Services.Customers;
using Nop.Services.Messages;
using Nop.Services.Seo;
using Nop.Services.Vendors;
using Nop.Services.Security;
using Nop.Services.Affiliates;
using Nop.Services.Authentication.External;
using Nop.Services.Catalog;
using Nop.Services.Common;
using Nop.Services.Directory;
using Nop.Services.ExportImport;
using Nop.Services.Forums;
using Nop.Services.Helpers;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Orders;
using Nop.Services.Stores;
using Nop.Services.Tax;
using Nop.Services.Authentication;
using Nop.Services.Events;
using Nop.Web.Framework.Security;
using Nop.Web.Framework.Security.Captcha;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework;
using Nop.Web.Models.Customer;
using Nop.Web.Models.Vendors;
using WebGrease.Css.Extensions;

namespace Nop.Web.Controllers
{
    public partial class VendorController : BasePublicController
    {
        #region Fields

        private readonly IWorkContext _workContext;
        private readonly ILocalizationService _localizationService;
        private readonly ICustomerService _customerService;
        private readonly IWorkflowMessageService _workflowMessageService;
        private readonly IVendorService _vendorService;
        private readonly IUrlRecordService _urlRecordService;
        private readonly CustomerSettings _customerSettings;
        private readonly DateTimeSettings _dateTimeSettings;
        private readonly INewsLetterSubscriptionService _newsLetterSubscriptionService;
        private readonly IGenericAttributeService _genericAttributeService;
        private readonly ICustomerRegistrationService _customerRegistrationService;
        private readonly IDateTimeHelper _dateTimeHelper;
        private readonly ICountryService _countryService;
        private readonly IStateProvinceService _stateProvinceService;
        private readonly IAddressService _addressService;
        private readonly ITaxService _taxService;
        private readonly IStoreContext _storeContext;
        private readonly IOpenAuthenticationService _openAuthenticationService;
        private readonly ICustomerAttributeParser _customerAttributeParser;
        private readonly ICustomerAttributeService _customerAttributeService;
        private readonly LocalizationSettings _localizationSettings;
        private readonly VendorSettings _vendorSettings;
        private readonly CaptchaSettings _captchaSettings;
        private readonly TaxSettings _taxSettings;
        private readonly SecuritySettings _securitySettings;
        private readonly IAuthenticationService _authenticationService;
        private readonly IEventPublisher _eventPublisher;
        private readonly IWebHelper _webHelper;

        #endregion

        #region Constructors

        public VendorController(IWorkContext workContext,
            ILocalizationService localizationService,
            ICustomerService customerService,
            IWorkflowMessageService workflowMessageService,
            IVendorService vendorService,
            IUrlRecordService urlRecordService,
            CustomerSettings customerSettings,
            DateTimeSettings dateTimeSettings,
            INewsLetterSubscriptionService newsLetterSubscriptionService,
            IGenericAttributeService genericAttributeService,
            ICustomerRegistrationService customerRegistrationService,
            IDateTimeHelper dateTimeHelper,
            ICountryService countryService,
            IStateProvinceService stateProvinceService,
            IAddressService addressService,
            ITaxService taxService,
            IStoreContext storeContext,
            IOpenAuthenticationService openAuthenticationService,
            ICustomerAttributeParser customerAttributeParser,
            ICustomerAttributeService customerAttributeService,
            LocalizationSettings localizationSettings,
            VendorSettings vendorSettings,
            CaptchaSettings captchaSettings,
            TaxSettings taxSettings,
            SecuritySettings securitySettings,
            IAuthenticationService authenticationService,
            IEventPublisher eventPublisher,
            IWebHelper webHelper
            )
        {
            this._workContext = workContext;
            this._localizationService = localizationService;
            this._customerService = customerService;
            this._workflowMessageService = workflowMessageService;
            this._vendorService = vendorService;
            this._urlRecordService = urlRecordService;
            this._customerSettings = customerSettings;
            this._dateTimeSettings = dateTimeSettings;
            this._newsLetterSubscriptionService = newsLetterSubscriptionService;
            this._genericAttributeService = genericAttributeService;
            this._customerRegistrationService = customerRegistrationService;
            this._dateTimeHelper = dateTimeHelper;
            this._countryService = countryService;
            this._stateProvinceService = stateProvinceService;
            this._addressService = addressService;
            this._taxService = taxService;
            this._storeContext = storeContext;
            this._openAuthenticationService = openAuthenticationService;
            this._customerAttributeParser = customerAttributeParser;
            this._customerAttributeService = customerAttributeService;
            this._localizationSettings = localizationSettings;
            this._vendorSettings = vendorSettings;
            this._captchaSettings = captchaSettings;
            this._taxSettings = taxSettings;
            this._securitySettings = securitySettings;
            this._authenticationService = authenticationService;
            this._eventPublisher = eventPublisher;
            this._webHelper = webHelper;
        }

        #endregion

        #region Utilities

        [NonAction]
        protected virtual void TryAssociateAccountWithExternalAccount(Customer customer)
        {
            var parameters = ExternalAuthorizerHelper.RetrieveParametersFromRoundTrip(true);
            if (parameters == null)
                return;

            if (_openAuthenticationService.AccountExists(parameters))
                return;

            _openAuthenticationService.AssociateExternalAccountWithUser(customer, parameters);
        }

        [NonAction]
        protected virtual IList<CustomerAttributeModel> PrepareCustomCustomerAttributes(Customer customer,
            string overrideAttributesXml = "")
        {
            if (customer == null)
                throw new ArgumentNullException("customer");

            var result = new List<CustomerAttributeModel>();

            var customerAttributes = _customerAttributeService.GetAllCustomerAttributes();
            foreach (var attribute in customerAttributes)
            {
                var attributeModel = new CustomerAttributeModel
                {
                    Id = attribute.Id,
                    Name = attribute.GetLocalized(x => x.Name),
                    IsRequired = attribute.IsRequired,
                    AttributeControlType = attribute.AttributeControlType,
                };

                if (attribute.ShouldHaveValues())
                {
                    //values
                    var attributeValues = _customerAttributeService.GetCustomerAttributeValues(attribute.Id);
                    foreach (var attributeValue in attributeValues)
                    {
                        var valueModel = new CustomerAttributeValueModel
                        {
                            Id = attributeValue.Id,
                            Name = attributeValue.GetLocalized(x => x.Name),
                            IsPreSelected = attributeValue.IsPreSelected
                        };
                        attributeModel.Values.Add(valueModel);
                    }
                }

                //set already selected attributes
                var selectedAttributesXml = !String.IsNullOrEmpty(overrideAttributesXml) ?
                    overrideAttributesXml :
                    customer.GetAttribute<string>(SystemCustomerAttributeNames.CustomCustomerAttributes, _genericAttributeService);
                switch (attribute.AttributeControlType)
                {
                    case AttributeControlType.DropdownList:
                    case AttributeControlType.RadioList:
                    case AttributeControlType.Checkboxes:
                        {
                            if (!String.IsNullOrEmpty(selectedAttributesXml))
                            {
                                //clear default selection
                                foreach (var item in attributeModel.Values)
                                    item.IsPreSelected = false;

                                //select new values
                                var selectedValues = _customerAttributeParser.ParseCustomerAttributeValues(selectedAttributesXml);
                                foreach (var attributeValue in selectedValues)
                                    foreach (var item in attributeModel.Values)
                                        if (attributeValue.Id == item.Id)
                                            item.IsPreSelected = true;
                            }
                        }
                        break;
                    case AttributeControlType.ReadonlyCheckboxes:
                        {
                            //do nothing
                            //values are already pre-set
                        }
                        break;
                    case AttributeControlType.TextBox:
                    case AttributeControlType.MultilineTextbox:
                        {
                            if (!String.IsNullOrEmpty(selectedAttributesXml))
                            {
                                var enteredText = _customerAttributeParser.ParseValues(selectedAttributesXml, attribute.Id);
                                if (enteredText.Any())
                                    attributeModel.DefaultValue = enteredText[0];
                            }
                        }
                        break;
                    case AttributeControlType.ColorSquares:
                    case AttributeControlType.ImageSquares:
                    case AttributeControlType.Datepicker:
                    case AttributeControlType.FileUpload:
                    default:
                        //not supported attribute control types
                        break;
                }

                result.Add(attributeModel);
            }


            return result;
        }

        [NonAction]
        protected virtual void PrepareCustomerRegisterModel(ApplyVendorModel model, bool excludeProperties,
            string overrideCustomCustomerAttributesXml = "")
        {
            if (model.RegisterModel == null)
                throw new ArgumentNullException("model");

            model.RegisterModel.AllowCustomersToSetTimeZone = _dateTimeSettings.AllowCustomersToSetTimeZone;
            foreach (var tzi in _dateTimeHelper.GetSystemTimeZones())
                model.RegisterModel.AvailableTimeZones.Add(new SelectListItem { Text = tzi.DisplayName, Value = tzi.Id, Selected = (excludeProperties ? tzi.Id == model.RegisterModel.TimeZoneId : tzi.Id == _dateTimeHelper.CurrentTimeZone.Id) });

            model.RegisterModel.DisplayVatNumber = _taxSettings.EuVatEnabled;
            //form fields
            model.RegisterModel.GenderEnabled = _customerSettings.GenderEnabled;
            model.RegisterModel.DateOfBirthEnabled = _customerSettings.DateOfBirthEnabled;
            model.RegisterModel.DateOfBirthRequired = _customerSettings.DateOfBirthRequired;
            model.RegisterModel.CompanyEnabled = _customerSettings.CompanyEnabled;
            model.RegisterModel.CompanyRequired = _customerSettings.CompanyRequired;
            model.RegisterModel.StreetAddressEnabled = _customerSettings.StreetAddressEnabled;
            model.RegisterModel.StreetAddressRequired = _customerSettings.StreetAddressRequired;
            model.RegisterModel.StreetAddress2Enabled = _customerSettings.StreetAddress2Enabled;
            model.RegisterModel.StreetAddress2Required = _customerSettings.StreetAddress2Required;
            model.RegisterModel.ZipPostalCodeEnabled = _customerSettings.ZipPostalCodeEnabled;
            model.RegisterModel.ZipPostalCodeRequired = _customerSettings.ZipPostalCodeRequired;
            model.RegisterModel.CityEnabled = _customerSettings.CityEnabled;
            model.RegisterModel.CityRequired = _customerSettings.CityRequired;
            model.RegisterModel.CountryEnabled = _customerSettings.CountryEnabled;
            model.RegisterModel.CountryRequired = _customerSettings.CountryRequired;
            model.RegisterModel.StateProvinceEnabled = _customerSettings.StateProvinceEnabled;
            model.RegisterModel.StateProvinceRequired = _customerSettings.StateProvinceRequired;
            model.RegisterModel.PhoneEnabled = _customerSettings.PhoneEnabled;
            model.RegisterModel.PhoneRequired = _customerSettings.PhoneRequired;
            model.RegisterModel.FaxEnabled = _customerSettings.FaxEnabled;
            model.RegisterModel.FaxRequired = _customerSettings.FaxRequired;
            model.RegisterModel.NewsletterEnabled = _customerSettings.NewsletterEnabled;
            model.RegisterModel.AcceptPrivacyPolicyEnabled = _customerSettings.AcceptPrivacyPolicyEnabled;
            model.RegisterModel.UsernamesEnabled = _customerSettings.UsernamesEnabled;
            model.RegisterModel.CheckUsernameAvailabilityEnabled = _customerSettings.CheckUsernameAvailabilityEnabled;
            model.RegisterModel.HoneypotEnabled = _securitySettings.HoneypotEnabled;
            model.RegisterModel.DisplayCaptcha = _captchaSettings.Enabled && _captchaSettings.ShowOnRegistrationPage;
            model.RegisterModel.EnteringEmailTwice = _customerSettings.EnteringEmailTwice;

            //countries and states
            if (_customerSettings.CountryEnabled)
            {
                //model.RegisterModel.AvailableCountries.Add(new SelectListItem { Text = _localizationService.GetResource("Address.SelectCountry"), Value = "0" });

                foreach (var c in _countryService.GetAllCountries(_workContext.WorkingLanguage.Id))
                {
                    model.RegisterModel.AvailableCountries.Add(new SelectListItem
                    {
                        Text = c.GetLocalized(x => x.Name),
                        Value = c.Id.ToString(),
                        Selected = c.Id == model.RegisterModel.CountryId
                    });
                }

                if (_customerSettings.StateProvinceEnabled)
                {
                    //states
                    var states = _stateProvinceService.GetStateProvincesByCountryId(model.RegisterModel.CountryId, _workContext.WorkingLanguage.Id).ToList();
                    if (states.Any())
                    {
                        model.RegisterModel.AvailableStates.Add(new SelectListItem { Text = _localizationService.GetResource("Address.SelectState"), Value = "0" });

                        foreach (var s in states)
                        {
                            model.RegisterModel.AvailableStates.Add(new SelectListItem { Text = s.GetLocalized(x => x.Name), Value = s.Id.ToString(), Selected = (s.Id == model.RegisterModel.StateProvinceId) });
                        }
                    }
                    else
                    {
                        bool anyCountrySelected = model.RegisterModel.AvailableCountries.Any(x => x.Selected);

                        model.RegisterModel.AvailableStates.Add(new SelectListItem
                        {
                            Text = _localizationService.GetResource(anyCountrySelected ? "Address.OtherNonUS" : "Address.SelectState"),
                            Value = "0"
                        });
                    }

                }
            }

            //custom customer attributes
            var customAttributes = PrepareCustomCustomerAttributes(_workContext.CurrentCustomer, overrideCustomCustomerAttributesXml);
            customAttributes.ForEach(model.RegisterModel.CustomerAttributes.Add);
        }

        [NonAction]
        protected virtual string ParseCustomCustomerAttributes(FormCollection form)
        {
            if (form == null)
                throw new ArgumentNullException("form");

            string attributesXml = "";
            var attributes = _customerAttributeService.GetAllCustomerAttributes();
            foreach (var attribute in attributes)
            {
                string controlId = string.Format("customer_attribute_{0}", attribute.Id);
                switch (attribute.AttributeControlType)
                {
                    case AttributeControlType.DropdownList:
                    case AttributeControlType.RadioList:
                        {
                            var ctrlAttributes = form[controlId];
                            if (!String.IsNullOrEmpty(ctrlAttributes))
                            {
                                int selectedAttributeId = int.Parse(ctrlAttributes);
                                if (selectedAttributeId > 0)
                                    attributesXml = _customerAttributeParser.AddCustomerAttribute(attributesXml,
                                        attribute, selectedAttributeId.ToString());
                            }
                        }
                        break;
                    case AttributeControlType.Checkboxes:
                        {
                            var cblAttributes = form[controlId];
                            if (!String.IsNullOrEmpty(cblAttributes))
                            {
                                foreach (var item in cblAttributes.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
                                {
                                    int selectedAttributeId = int.Parse(item);
                                    if (selectedAttributeId > 0)
                                        attributesXml = _customerAttributeParser.AddCustomerAttribute(attributesXml,
                                            attribute, selectedAttributeId.ToString());
                                }
                            }
                        }
                        break;
                    case AttributeControlType.ReadonlyCheckboxes:
                        {
                            //load read-only (already server-side selected) values
                            var attributeValues = _customerAttributeService.GetCustomerAttributeValues(attribute.Id);
                            foreach (var selectedAttributeId in attributeValues
                                .Where(v => v.IsPreSelected)
                                .Select(v => v.Id)
                                .ToList())
                            {
                                attributesXml = _customerAttributeParser.AddCustomerAttribute(attributesXml,
                                            attribute, selectedAttributeId.ToString());
                            }
                        }
                        break;
                    case AttributeControlType.TextBox:
                    case AttributeControlType.MultilineTextbox:
                        {
                            var ctrlAttributes = form[controlId];
                            if (!String.IsNullOrEmpty(ctrlAttributes))
                            {
                                string enteredText = ctrlAttributes.Trim();
                                attributesXml = _customerAttributeParser.AddCustomerAttribute(attributesXml,
                                    attribute, enteredText);
                            }
                        }
                        break;
                    case AttributeControlType.Datepicker:
                    case AttributeControlType.ColorSquares:
                    case AttributeControlType.ImageSquares:
                    case AttributeControlType.FileUpload:
                    //not supported customer attributes
                    default:
                        break;
                }
            }

            return attributesXml;
        }

        #endregion

        #region Methods

        [NopHttpsRequirement(SslRequirement.Yes)]
        public ActionResult ApplyVendor()
        {
            if (!_vendorSettings.AllowCustomersToApplyForVendorAccount)
                return RedirectToRoute("HomePage");

            var model = new ApplyVendorModel();
            var registermodel = new RegisterModel();

            if (_workContext.CurrentCustomer.VendorId > 0)
            {
                //already applied for vendor account
                model.DisableFormInput = true;
                model.Result = _localizationService.GetResource("Vendors.ApplyAccount.AlreadyApplied");
                return View(model);
            }
            if (!_workContext.CurrentCustomer.IsRegistered())
            {
                //customer is not registered
                model.RegisterModel = registermodel;
                ViewBag.validEmail = 0;
            }
            else
            {
                //customer is registered
                var customer = _workContext.CurrentCustomer;
                if (customer.BillingAddress != null)
                {
                    registermodel.FirstName = customer.BillingAddress.FirstName;
                    registermodel.LastName = customer.BillingAddress.LastName;
                    registermodel.Company = customer.BillingAddress.Company;
                    registermodel.StreetAddress = customer.BillingAddress.Address1;
                    registermodel.ZipPostalCode = customer.BillingAddress.ZipPostalCode;
                    registermodel.City = customer.BillingAddress.City;
                    registermodel.Phone = customer.BillingAddress.PhoneNumber;
                }

                registermodel.Email = customer.Email;
                registermodel.Password = customer.Password;
                registermodel.ConfirmPassword = customer.Password;

                model.RegisterModel = registermodel;
                model.Email = customer.Email;
                ViewBag.validEmail = 1;
            }

            PrepareCustomerRegisterModel(model, false);

            //enable newsletter by default
            model.RegisterModel.Newsletter = _customerSettings.NewsletterTickedByDefault;

            model.DisplayCaptcha = _captchaSettings.Enabled && _captchaSettings.ShowOnApplyVendorPage;
            return View(model);
        }

        [HttpPost, ActionName("ApplyVendor")]
        [PublicAntiForgery]
        [CaptchaValidator]
        public ActionResult ApplyVendorSubmit(ApplyVendorModel model, bool captchaValid, FormCollection form)
        {
            if (!_vendorSettings.AllowCustomersToApplyForVendorAccount)
                return RedirectToRoute("HomePage");

            //if (!_workContext.CurrentCustomer.IsRegistered())
            //return new HttpUnauthorizedResult();

            var customer1 = _workContext.CurrentCustomer;

            //custom customer attributes
            var customerAttributesXml = ParseCustomCustomerAttributes(form);
            var customerAttributeWarnings = _customerAttributeParser.GetAttributeWarnings(customerAttributesXml);
            foreach (var error in customerAttributeWarnings)
            {
                ModelState.AddModelError("", error);
            }

            //validate CAPTCHA
            if (_captchaSettings.Enabled && _captchaSettings.ShowOnRegistrationPage && !captchaValid)
            {
                ModelState.AddModelError("", _captchaSettings.GetWrongCaptchaMessage(_localizationService));
            }

            if (ModelState.IsValid)
            {
                if (!_workContext.CurrentCustomer.IsRegistered())
                {
                    if (_customerSettings.UsernamesEnabled && model.RegisterModel.Username != null)
                    {
                        model.RegisterModel.Username = model.RegisterModel.Username.Trim();
                    }

                    bool isApproved = _customerSettings.UserRegistrationType == UserRegistrationType.Standard;
                    var registrationRequest = new CustomerRegistrationRequest(customer1,
                        model.Email,
                        _customerSettings.UsernamesEnabled ? model.RegisterModel.Username : model.Email,
                        model.RegisterModel.Password,
                        _customerSettings.DefaultPasswordFormat,
                        _storeContext.CurrentStore.Id,
                        isApproved);
                    var registrationResult = _customerRegistrationService.RegisterCustomer(registrationRequest);

                    if (registrationResult.Success)
                    {
                        //properties
                        if (_dateTimeSettings.AllowCustomersToSetTimeZone)
                        {
                            _genericAttributeService.SaveAttribute(customer1, SystemCustomerAttributeNames.TimeZoneId, model.RegisterModel.TimeZoneId);
                        }
                        //VAT number
                        if (_taxSettings.EuVatEnabled)
                        {
                            _genericAttributeService.SaveAttribute(customer1, SystemCustomerAttributeNames.VatNumber, model.RegisterModel.VatNumber);

                            string vatName;
                            string vatAddress;
                            var vatNumberStatus = _taxService.GetVatNumberStatus(model.RegisterModel.VatNumber, out vatName, out vatAddress);
                            _genericAttributeService.SaveAttribute(customer1,
                                SystemCustomerAttributeNames.VatNumberStatusId,
                                (int)vatNumberStatus);
                            //send VAT number admin notification
                            if (!String.IsNullOrEmpty(model.RegisterModel.VatNumber) && _taxSettings.EuVatEmailAdminWhenNewVatSubmitted)
                                _workflowMessageService.SendNewVatSubmittedStoreOwnerNotification(customer1, model.RegisterModel.VatNumber, vatAddress, _localizationSettings.DefaultAdminLanguageId);

                        }

                        //form fields
                        if (_customerSettings.GenderEnabled)
                            _genericAttributeService.SaveAttribute(customer1, SystemCustomerAttributeNames.Gender, model.RegisterModel.Gender);
                        _genericAttributeService.SaveAttribute(customer1, SystemCustomerAttributeNames.FirstName, model.RegisterModel.FirstName);
                        _genericAttributeService.SaveAttribute(customer1, SystemCustomerAttributeNames.LastName, model.RegisterModel.LastName);
                        if (_customerSettings.DateOfBirthEnabled)
                        {
                            DateTime? dateOfBirth = model.RegisterModel.ParseDateOfBirth();
                            _genericAttributeService.SaveAttribute(customer1, SystemCustomerAttributeNames.DateOfBirth, dateOfBirth);
                        }
                        if (_customerSettings.CompanyEnabled)
                            _genericAttributeService.SaveAttribute(customer1, SystemCustomerAttributeNames.Company, model.RegisterModel.Company);
                        if (_customerSettings.StreetAddressEnabled)
                            _genericAttributeService.SaveAttribute(customer1, SystemCustomerAttributeNames.StreetAddress, model.RegisterModel.StreetAddress);
                        if (_customerSettings.StreetAddress2Enabled)
                            _genericAttributeService.SaveAttribute(customer1, SystemCustomerAttributeNames.StreetAddress2, model.RegisterModel.StreetAddress2);
                        if (_customerSettings.ZipPostalCodeEnabled)
                            _genericAttributeService.SaveAttribute(customer1, SystemCustomerAttributeNames.ZipPostalCode, model.RegisterModel.ZipPostalCode);
                        if (_customerSettings.CityEnabled)
                            _genericAttributeService.SaveAttribute(customer1, SystemCustomerAttributeNames.City, model.RegisterModel.City);
                        if (_customerSettings.CountryEnabled)
                            _genericAttributeService.SaveAttribute(customer1, SystemCustomerAttributeNames.CountryId, model.RegisterModel.CountryId);
                        if (_customerSettings.CountryEnabled && _customerSettings.StateProvinceEnabled)
                            _genericAttributeService.SaveAttribute(customer1, SystemCustomerAttributeNames.StateProvinceId, model.RegisterModel.StateProvinceId);
                        if (_customerSettings.PhoneEnabled)
                            _genericAttributeService.SaveAttribute(customer1, SystemCustomerAttributeNames.Phone, model.RegisterModel.Phone);
                        if (_customerSettings.FaxEnabled)
                            _genericAttributeService.SaveAttribute(customer1, SystemCustomerAttributeNames.Fax, model.RegisterModel.Fax);

                        //newsletter
                        if (_customerSettings.NewsletterEnabled)
                        {
                            //save newsletter value
                            var newsletter = _newsLetterSubscriptionService.GetNewsLetterSubscriptionByEmailAndStoreId(model.RegisterModel.Email, _storeContext.CurrentStore.Id);
                            if (newsletter != null)
                            {
                                if (model.RegisterModel.Newsletter)
                                {
                                    newsletter.Active = true;
                                    _newsLetterSubscriptionService.UpdateNewsLetterSubscription(newsletter);
                                }
                                //else
                                //{
                                //When registering, not checking the newsletter check box should not take an existing email address off of the subscription list.
                                //_newsLetterSubscriptionService.DeleteNewsLetterSubscription(newsletter);
                                //}
                            }
                            else
                            {
                                if (model.RegisterModel.Newsletter)
                                {
                                    _newsLetterSubscriptionService.InsertNewsLetterSubscription(new NewsLetterSubscription
                                    {
                                        NewsLetterSubscriptionGuid = Guid.NewGuid(),
                                        Email = model.RegisterModel.Email,
                                        Active = true,
                                        StoreId = _storeContext.CurrentStore.Id,
                                        CreatedOnUtc = DateTime.UtcNow
                                    });
                                }
                            }
                        }

                        //save customer attributes
                        _genericAttributeService.SaveAttribute(customer1, SystemCustomerAttributeNames.CustomCustomerAttributes, customerAttributesXml);

                        //login customer now
                        if (isApproved)
                            _authenticationService.SignIn(customer1, true);

                        //associated with external account (if possible)
                        TryAssociateAccountWithExternalAccount(customer1);

                        //insert default address (if possible)
                        var defaultAddress = new Address
                        {
                            FirstName = customer1.GetAttribute<string>(SystemCustomerAttributeNames.FirstName),
                            LastName = customer1.GetAttribute<string>(SystemCustomerAttributeNames.LastName),
                            Email = customer1.Email,
                            Company = customer1.GetAttribute<string>(SystemCustomerAttributeNames.Company),
                            CountryId = customer1.GetAttribute<int>(SystemCustomerAttributeNames.CountryId) > 0 ?
                                (int?)customer1.GetAttribute<int>(SystemCustomerAttributeNames.CountryId) : null,
                            StateProvinceId = customer1.GetAttribute<int>(SystemCustomerAttributeNames.StateProvinceId) > 0 ?
                                (int?)customer1.GetAttribute<int>(SystemCustomerAttributeNames.StateProvinceId) : null,
                            City = customer1.GetAttribute<string>(SystemCustomerAttributeNames.City),
                            Address1 = customer1.GetAttribute<string>(SystemCustomerAttributeNames.StreetAddress),
                            Address2 = customer1.GetAttribute<string>(SystemCustomerAttributeNames.StreetAddress2),
                            ZipPostalCode = customer1.GetAttribute<string>(SystemCustomerAttributeNames.ZipPostalCode),
                            PhoneNumber = customer1.GetAttribute<string>(SystemCustomerAttributeNames.Phone),
                            FaxNumber = customer1.GetAttribute<string>(SystemCustomerAttributeNames.Fax),
                            CreatedOnUtc = customer1.CreatedOnUtc
                        };
                        if (this._addressService.IsAddressValid(defaultAddress))
                        {
                            //some validation
                            if (defaultAddress.CountryId == 0)
                                defaultAddress.CountryId = null;
                            if (defaultAddress.StateProvinceId == 0)
                                defaultAddress.StateProvinceId = null;
                            //set default address
                            customer1.Addresses.Add(defaultAddress);
                            customer1.BillingAddress = defaultAddress;
                            customer1.ShippingAddress = defaultAddress;
                            _customerService.UpdateCustomer(customer1);
                        }

                        //notifications
                        if (_customerSettings.NotifyNewCustomerRegistration)
                            _workflowMessageService.SendCustomerRegisteredNotificationMessage(customer1, _localizationSettings.DefaultAdminLanguageId);

                        //raise event       
                        _eventPublisher.Publish(new CustomerRegisteredEvent(customer1));
                    }
                    //errors
                    foreach (var error in registrationResult.Errors)
                    {
                        ModelState.AddModelError("", error);
                        if (error != null)
                            //If we got this far, something failed, redisplay form
                            PrepareCustomerRegisterModel(model, true);
                        model.DisplayCaptcha = _captchaSettings.Enabled && _captchaSettings.ShowOnApplyVendorPage;
                        ViewBag.validEmail = 0;
                        return View(model);
                    }
                }
                else
                {
                    //form fields
                    if (_customerSettings.GenderEnabled)
                        _genericAttributeService.SaveAttribute(customer1, SystemCustomerAttributeNames.Gender, model.RegisterModel.Gender);
                    _genericAttributeService.SaveAttribute(customer1, SystemCustomerAttributeNames.FirstName, model.RegisterModel.FirstName);
                    _genericAttributeService.SaveAttribute(customer1, SystemCustomerAttributeNames.LastName, model.RegisterModel.LastName);
                    if (_customerSettings.DateOfBirthEnabled)
                    {
                        DateTime? dateOfBirth = model.RegisterModel.ParseDateOfBirth();
                        _genericAttributeService.SaveAttribute(customer1, SystemCustomerAttributeNames.DateOfBirth, dateOfBirth);
                    }
                    if (_customerSettings.CompanyEnabled)
                        _genericAttributeService.SaveAttribute(customer1, SystemCustomerAttributeNames.Company, model.RegisterModel.Company);
                    if (_customerSettings.StreetAddressEnabled)
                        _genericAttributeService.SaveAttribute(customer1, SystemCustomerAttributeNames.StreetAddress, model.RegisterModel.StreetAddress);
                    if (_customerSettings.StreetAddress2Enabled)
                        _genericAttributeService.SaveAttribute(customer1, SystemCustomerAttributeNames.StreetAddress2, model.RegisterModel.StreetAddress2);
                    if (_customerSettings.ZipPostalCodeEnabled)
                        _genericAttributeService.SaveAttribute(customer1, SystemCustomerAttributeNames.ZipPostalCode, model.RegisterModel.ZipPostalCode);
                    if (_customerSettings.CityEnabled)
                        _genericAttributeService.SaveAttribute(customer1, SystemCustomerAttributeNames.City, model.RegisterModel.City);
                    if (_customerSettings.CountryEnabled)
                        _genericAttributeService.SaveAttribute(customer1, SystemCustomerAttributeNames.CountryId, model.RegisterModel.CountryId);
                    if (_customerSettings.CountryEnabled && _customerSettings.StateProvinceEnabled)
                        _genericAttributeService.SaveAttribute(customer1, SystemCustomerAttributeNames.StateProvinceId, model.RegisterModel.StateProvinceId);
                    if (_customerSettings.PhoneEnabled)
                        _genericAttributeService.SaveAttribute(customer1, SystemCustomerAttributeNames.Phone, model.RegisterModel.Phone);
                    if (_customerSettings.FaxEnabled)
                        _genericAttributeService.SaveAttribute(customer1, SystemCustomerAttributeNames.Fax, model.RegisterModel.Fax);

                    //newsletter
                    if (_customerSettings.NewsletterEnabled)
                    {
                        //save newsletter value
                        var newsletter = _newsLetterSubscriptionService.GetNewsLetterSubscriptionByEmailAndStoreId(model.RegisterModel.Email, _storeContext.CurrentStore.Id);
                        if (newsletter != null)
                        {
                            if (model.RegisterModel.Newsletter)
                            {
                                newsletter.Active = true;
                                _newsLetterSubscriptionService.UpdateNewsLetterSubscription(newsletter);
                            }
                            //else
                            //{
                            //When registering, not checking the newsletter check box should not take an existing email address off of the subscription list.
                            //_newsLetterSubscriptionService.DeleteNewsLetterSubscription(newsletter);
                            //}
                        }
                        else
                        {
                            if (model.RegisterModel.Newsletter)
                            {
                                _newsLetterSubscriptionService.InsertNewsLetterSubscription(new NewsLetterSubscription
                                {
                                    NewsLetterSubscriptionGuid = Guid.NewGuid(),
                                    Email = customer1.Email,
                                    Active = true,
                                    StoreId = _storeContext.CurrentStore.Id,
                                    CreatedOnUtc = DateTime.UtcNow
                                });
                            }
                        }
                    }

                    //save customer attributes
                    _genericAttributeService.SaveAttribute(customer1, SystemCustomerAttributeNames.CustomCustomerAttributes, customerAttributesXml);

                    //insert default address (if possible)
                    var defaultAddress = new Address
                    {
                        FirstName = customer1.GetAttribute<string>(SystemCustomerAttributeNames.FirstName),
                        LastName = customer1.GetAttribute<string>(SystemCustomerAttributeNames.LastName),
                        Email = customer1.Email,
                        Company = customer1.GetAttribute<string>(SystemCustomerAttributeNames.Company),
                        CountryId = customer1.GetAttribute<int>(SystemCustomerAttributeNames.CountryId) > 0 ?
                            (int?)customer1.GetAttribute<int>(SystemCustomerAttributeNames.CountryId) : null,
                        StateProvinceId = customer1.GetAttribute<int>(SystemCustomerAttributeNames.StateProvinceId) > 0 ?
                            (int?)customer1.GetAttribute<int>(SystemCustomerAttributeNames.StateProvinceId) : null,
                        City = customer1.GetAttribute<string>(SystemCustomerAttributeNames.City),
                        Address1 = customer1.GetAttribute<string>(SystemCustomerAttributeNames.StreetAddress),
                        Address2 = customer1.GetAttribute<string>(SystemCustomerAttributeNames.StreetAddress2),
                        ZipPostalCode = customer1.GetAttribute<string>(SystemCustomerAttributeNames.ZipPostalCode),
                        PhoneNumber = customer1.GetAttribute<string>(SystemCustomerAttributeNames.Phone),
                        FaxNumber = customer1.GetAttribute<string>(SystemCustomerAttributeNames.Fax),
                        CreatedOnUtc = customer1.CreatedOnUtc
                    };
                    if (this._addressService.IsAddressValid(defaultAddress))
                    {
                        //some validation
                        if (defaultAddress.CountryId == 0)
                            defaultAddress.CountryId = null;
                        if (defaultAddress.StateProvinceId == 0)
                            defaultAddress.StateProvinceId = null;
                        //set default address

                        var address = customer1.BillingAddress;
                        customer1.RemoveAddress(customer1.BillingAddress);
                        _customerService.UpdateCustomer(customer1);

                        _addressService.DeleteAddress(address);

                        customer1.Addresses.Add(defaultAddress);
                        customer1.BillingAddress = defaultAddress;
                        customer1.ShippingAddress = defaultAddress;
                        _customerService.UpdateCustomer(customer1);

                    }

                    //notifications
                    if (_customerSettings.NotifyNewCustomerRegistration)
                        _workflowMessageService.SendCustomerRegisteredNotificationMessage(customer1, _localizationSettings.DefaultAdminLanguageId);

                    //raise event       
                    _eventPublisher.Publish(new CustomerRegisteredEvent(customer1));
                    //customer1.RemoveAddress(customer1.BillingAddress);
                }


                //Get customer id
                var custid = _workContext.CurrentCustomer.Id;

                //validate CAPTCHA
                if (_captchaSettings.Enabled && _captchaSettings.ShowOnApplyVendorPage && !captchaValid)
                {
                    ModelState.AddModelError("", _localizationService.GetResource("Common.WrongCaptcha"));
                }

                //disabled by default
                var vendor = new Vendor
                {
                    Name = model.Name,
                    Email = model.Email,

                    //some default settings
                    PageSize = 6,
                    AllowCustomersToSelectPageSize = true,
                    PageSizeOptions = _vendorSettings.DefaultVendorPageSizeOptions,
                    Active = true,
                    Description = model.Description,
                    BankAccountNumber = model.BankAccountNumber,
                    CVR = model.CVR,
                    CPR = model.CPR,
                    CompanyStructure = model.CompanyStructure
                };

                _vendorService.InsertVendor(vendor);
                //search engine name (the same as vendor name)
                var seName = vendor.ValidateSeName(vendor.Name, vendor.Name, true);
                _urlRecordService.SaveSlug(vendor, seName, 0);

                //associate to the current customer
                //but a store owner will have to manually add this customer role to "Vendors" role
                //if he wants to grant access to admin area
                _workContext.CurrentCustomer.VendorId = vendor.Id;
                _customerService.UpdateCustomer(_workContext.CurrentCustomer);

                //To assign customer role to the user as a vendor
                //Using customer id for updating customer
                var customer = _customerService.GetCustomerById(custid);
                var registeredRole = _customerService.GetCustomerRoleBySystemName(SystemCustomerRoleNames.Vendors);
                customer.CustomerRoles.Add(registeredRole);
                customer.Active = true;
                _customerService.UpdateCustomer(customer);

                //notify store owner here (email)
                _workflowMessageService.SendNewVendorAccountApplyStoreOwnerNotification(_workContext.CurrentCustomer,
                    vendor, _localizationSettings.DefaultAdminLanguageId);

                model.DisableFormInput = true;
                model.Result = _localizationService.GetResource("Vendors.ApplyAccount.Submitted");
                return View(model);
            }

            //If we got this far, something failed, redisplay form
            PrepareCustomerRegisterModel(model, true);
            model.DisplayCaptcha = _captchaSettings.Enabled && _captchaSettings.ShowOnApplyVendorPage;
            return View(model);
        }

        #endregion


    }
}
