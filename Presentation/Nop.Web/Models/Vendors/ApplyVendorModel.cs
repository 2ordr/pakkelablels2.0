﻿using System.Web.Mvc;
using FluentValidation.Attributes;
using Nop.Web.Framework;
using Nop.Web.Framework.Mvc;
using Nop.Web.Validators.Vendors;
using Nop.Web.Models.Customer;
using System.ComponentModel.DataAnnotations;

namespace Nop.Web.Models.Vendors
{
    [Validator(typeof(ApplyVendorValidator))]
    public partial class ApplyVendorModel : BaseNopModel
    {
        [NopResourceDisplayName("Vendors.ApplyAccount.StoreName")]
        [AllowHtml]
        public string Name { get; set; }

        [NopResourceDisplayName("Vendors.ApplyAccount.Email")]
        [AllowHtml]
        public string Email { get; set; }

        [NopResourceDisplayName("Vendors.ApplyAccount.SellerInformation")]
        [AllowHtml]
        public string Description { get; set; }

        public bool DisplayCaptcha { get; set; }

        public bool DisableFormInput { get; set; }

        public string Result { get; set; }

        public string CVR { get; set; }

        public string CPR { get; set; }

        [NopResourceDisplayName("Vendors.ApplyAccount.BankAccountNumber")]
        [AllowHtml]
        public string BankAccountNumber { get; set; }

        [NopResourceDisplayName("Vendors.ApplyAccount.CompanyStructure")]
        [AllowHtml]
        public string CompanyStructure { get; set; }

        public RegisterModel RegisterModel { get; set; }

    }
}