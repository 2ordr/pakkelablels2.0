﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Widgets.Like.Models
{
  public class LikeHeaderModel
    {
        public bool IsGuest { get; set; }
        public int Count { get; set; }
    }
}
