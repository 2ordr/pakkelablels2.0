﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Widgets.Like.Models
{
    public class ProductCount
    {
        public int Count { get; set; }
        public int ProductId { get; set; }
    }
}
