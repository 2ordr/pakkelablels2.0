﻿using Nop.Web.Models.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Widgets.Like.Models
{
    public class LikeRouteValues : IRouteValues
    {
        public int page { get; set; }
    }
}
