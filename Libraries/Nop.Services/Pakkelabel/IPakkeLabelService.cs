﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Routing;
using Nop.Core.Domain.Common;
using Nop.Core.Plugins;
using Nop.Core.Domain.Pakkelabel;

namespace Nop.Services.Pakkelabel
{
    public partial interface IPakkeLabelService
    {
        /// <summary>
        /// Gets a route for provider configuration
        /// </summary>
        /// <param name="actionName">Action name</param>
        /// <param name="controllerName">Controller name</param>
        /// <param name="routeValues">Route values</param>
        //  void GetConfigurationRoute(out string actionName, out string controllerName, out RouteValueDictionary routeValues);

        /// <summary>
        /// Gets all pakkelabel method
        /// </summary>
        /// <returns>Pakkelabel Method</returns>
        IList<PakkelabelGenerationMethod> GetAllPakkelabelMethod();

        /// <summary>
        /// Update Pakkelabel
        /// </summary>
        /// <param name="pakkelabelGenerationMethod">Shipment</param>
        void UpdatePakkeLabel(PakkelabelGenerationMethod pakkelabelGenerationMethod);

        /// <summary>
        /// Fetches the Current Methode Values
        /// </summary>
        /// <param name="PakkelabelId"></param>
        /// <returns></returns>
        PakkelabelGenerationMethod GetCurrentMethod(int PakkelabelId);

    }
}
