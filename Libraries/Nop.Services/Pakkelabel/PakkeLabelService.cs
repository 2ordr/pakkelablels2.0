﻿using System;
using System.Collections.Generic;
using System.Linq;
using Nop.Core;
using Nop.Core.Data;
using Nop.Core.Domain.Pakkelabel;
using Nop.Services.Events;

namespace Nop.Services.Pakkelabel
{
    /// <summary>
    /// Pakkelabel service
    /// </summary>
    /// 
    public partial class PakkeLabelService : IPakkeLabelService
    {
        #region Fields

        private readonly IRepository<PakkelabelGenerationMethod> _pakkelabelLabelRepository;
        private readonly IEventPublisher _eventPublisher;

        #endregion

        #region Ctor

        /// <summary>
        /// Ctor
        /// </summary>
        /// <param name="pakkelabelRepository">Pakkelabel repository</param>
        /// <param name="pkRepository">pakkelabel repository</param>
        /// <param name="eventPublisher">Event published</param>
        public PakkeLabelService(IRepository<PakkelabelGenerationMethod> pakkelabelLabelRepository,
            IEventPublisher eventPublisher)
        {
            this._pakkelabelLabelRepository = pakkelabelLabelRepository;
            this._eventPublisher = eventPublisher;
        }

        #endregion

        #region Methods


        public virtual IList<PakkelabelGenerationMethod> GetAllPakkelabelMethod()
        {
            var query = from PkLabel in _pakkelabelLabelRepository.Table
                        orderby PkLabel.Id
                        select PkLabel;
            return query.ToList();
        }

        /// <summary>
        /// Update Pakkelabel
        /// </summary>
        /// <param name="pakkelabel">Pakkelabel</param>
        public virtual void UpdatePakkeLabel(PakkelabelGenerationMethod pakkelabelGenerationMethod)
        {
            if (pakkelabelGenerationMethod == null)
                throw new ArgumentNullException("pakkelabelGenerationMethod");

            _pakkelabelLabelRepository.Update(pakkelabelGenerationMethod);

            //event notification
            _eventPublisher.EntityUpdated(pakkelabelGenerationMethod);

        }

        public virtual PakkelabelGenerationMethod GetCurrentMethod(int PakkelabelId)
        {
            if (PakkelabelId == 0)
                return null;

            return _pakkelabelLabelRepository.Table.First(x => x.Id == PakkelabelId);
        }

        #endregion
    }
}
