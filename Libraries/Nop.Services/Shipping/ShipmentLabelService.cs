using System;
using System.Collections.Generic;
using System.Linq;
using Nop.Core;
using Nop.Core.Data;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.Shipping;
using Nop.Services.Events;

namespace Nop.Services.Shipping
{
    /// <summary>
    /// Shipment service
    /// </summary>
   
    public partial class ShipmentLabelService : IShippingLabel
    {
        #region Fields

        private readonly IRepository<ShipmentLabel> _shipmentLabelRepository;
        private readonly IEventPublisher _eventPublisher;
        
        #endregion

        #region Ctor

        /// <summary>
        /// Ctor
        /// </summary>
        /// <param name="shipmentRepository">Shipment repository</param>
        /// <param name="siRepository">Shipment item repository</param>
        /// <param name="orderItemRepository">Order item repository</param>
        /// <param name="eventPublisher">Event published</param>
        public ShipmentLabelService(IRepository<ShipmentLabel> shipmentlabelRepository,
            IEventPublisher eventPublisher)
        {
            this._shipmentLabelRepository = shipmentlabelRepository;
            this._eventPublisher = eventPublisher;
        }

        #endregion

        #region Methods

     

        /// <summary>
        /// Inserts a shipment
        /// </summary>
        /// <param name="shipment">Shipment</param>
        public virtual void InsertShipmentLabel(ShipmentLabel shipmentlabel)
        {
            if (shipmentlabel == null)
                throw new ArgumentNullException("shipmentlabel");

            _shipmentLabelRepository.Insert(shipmentlabel);

            //event notification
            _eventPublisher.EntityInserted(shipmentlabel);
        }

        public virtual ShipmentLabel GetLabelPath(string TrackingNumber,int OrderId)
        {
            if (TrackingNumber == "")
                return null;

            return _shipmentLabelRepository.Table.First(x => x.TrackingNumber == TrackingNumber && x.OrderId == OrderId);

        }


        #endregion
    }
}
