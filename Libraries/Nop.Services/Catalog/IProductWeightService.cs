﻿using Nop.Core.Domain.Catalog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Services.Catalog
{
    public partial interface IProductWeightService
    {
        /// <summary>
        /// Gets productweight
        /// </summary>
        /// <param name="Id"> Default Product weight identifier</param>
        /// <returns>Product</returns>
        ProductWeight GetDefaultProductWeightById(int id);


        /// <summary>
        /// Gets all products weight
        /// </summary>
        /// <returns>Default Products weight</returns>
        IList<ProductWeight> GetAllDefaultProductWeight();
    }
}
