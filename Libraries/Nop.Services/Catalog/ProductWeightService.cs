﻿using Nop.Core.Caching;
using Nop.Core.Data;
using Nop.Core.Domain.Catalog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Services.Catalog
{
    /// <summary>
    /// Product weight service
    /// </summary>
    public partial class ProductWeightService : IProductWeightService
    {
        #region Fields

        private readonly IRepository<ProductWeight> _productWeightRepository;

        #endregion

        #region Ctor

        /// <summary>
        /// Ctor
        /// </summary>
        /// <param name="productWeightRepository">Product weight repository</param>
        public ProductWeightService(IRepository<ProductWeight> productWeightRepository)
        {
            this._productWeightRepository = productWeightRepository;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Gets product weight
        /// </summary>
        /// <param name="Id">Product weight identifier</param>
        /// <returns>weight</returns>
        public virtual ProductWeight GetDefaultProductWeightById(int id)
        {
            if (id == 0)
                return null;

            return  _productWeightRepository.GetById(id);
        }

        /// <summary>
        /// Gets Default products weight
        /// </summary>
        /// <returns>Products Weight</returns>
        public virtual IList<ProductWeight> GetAllDefaultProductWeight()
        {
            var query = from p in _productWeightRepository.Table
                        select p;
            var productsWeight = query.ToList();
            return productsWeight;
        }

        #endregion
    }
}
