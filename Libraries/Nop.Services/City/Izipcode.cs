﻿using System;
using System.Collections.Generic;
using Nop.Core;
using Nop.Core.Domain.Common;
using System.Web.Routing;
using Nop.Core.Plugins;
using Nop.Core.Domain.City;
using Nop.Services.City;

namespace Nop.Services.City
{
    public partial interface Izipcode
    {
        Zipcodes GetZipcodeCity(string code);
    }
}
