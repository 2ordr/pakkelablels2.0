﻿using System;
using System.Collections.Generic;
using System.Linq;
using Nop.Core;
using Nop.Core.Data;
using Nop.Core.Domain.Common;
using Nop.Core.Plugins;
using Nop.Core.Domain.City;
using Nop.Services.City;
using Nop.Services.Events;

namespace Nop.Services.City
{
    public partial class ZipcodeService : Izipcode
    {
        private readonly IRepository<Zipcodes> _zipcodeRepository;
        private readonly IEventPublisher _eventPublisher;

        public ZipcodeService(IRepository<Zipcodes> zipcodeRepository,
            IEventPublisher eventPublisher)
        {
            this._zipcodeRepository = zipcodeRepository;
            this._eventPublisher = eventPublisher;
        }

        public virtual Zipcodes GetZipcodeCity(string code)
        {
            if (code == "")
                return null;
            var query = from z in _zipcodeRepository.Table
                        where z.Zipcode== code
                        select z;
            var zipcode = query.FirstOrDefault();
            return zipcode;

         //  return _zipcodeRepository.Table.First(x => x.Zipcode.ToString() == code);
        }
    }
}
