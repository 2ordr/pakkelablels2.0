﻿using Nop.Core.Domain.City;

namespace Nop.Data.Mapping.City
{
    public partial class ZipcodeMap : NopEntityTypeConfiguration<Zipcodes>
    {
        public ZipcodeMap()
        {
            this.ToTable("Zipcodes");
            this.HasKey(s => s.Id);

            this.Property(s => s.Zipcode);
            this.Property(s => s.City);           
          
        }
    }
}
