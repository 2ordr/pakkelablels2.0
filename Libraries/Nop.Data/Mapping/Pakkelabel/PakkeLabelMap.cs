﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Core.Domain.Pakkelabel;

namespace Nop.Data.Mapping.Pakkelabel
{
   public partial class PakkeLabelMap : NopEntityTypeConfiguration<PakkelabelGenerationMethod>
    {
       public PakkeLabelMap()
        {
            this.ToTable("PakkelabelGenerationMethod");
            this.HasKey(s => s.Id);

            this.Property(s => s.Setting);
            this.Property(s => s.Value);
          
        }
    }
}
