﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Core.Domain.Catalog;

namespace Nop.Data.Mapping.Catalog
{
    public partial class ProductWeightMap : NopEntityTypeConfiguration<ProductWeight>
    {
        public ProductWeightMap()
        {
            this.ToTable("ProductWeight");
            this.HasKey(p => p.Id);
            this.Property(p => p.MinWeight).IsRequired().HasPrecision(18, 0);
            this.Property(p => p.MaxWeight).IsRequired().HasPrecision(18, 2);
            this.Property(p => p.Weight).IsRequired().HasPrecision(18, 4);
            this.Property(p => p.Length).HasPrecision(18, 4);
            this.Property(p => p.Width).HasPrecision(18, 4);
            this.Property(p => p.Height).HasPrecision(18, 4);

        }
    }
}
