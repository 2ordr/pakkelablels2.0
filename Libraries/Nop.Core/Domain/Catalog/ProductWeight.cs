﻿using Nop.Core.Domain.Localization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Core.Domain.Catalog
{
    /// <summary>
    /// Represents a product weight
    /// </summary>
    public partial class ProductWeight : BaseEntity, ILocalizedEntity
    {
        /// <summary>
        /// Gets or sets the Minweight
        /// </summary>
        public decimal MinWeight { get; set; }

        /// <summary>
        /// Gets or sets the Maxweight
        /// </summary>
        public decimal MaxWeight { get; set; }

        /// <summary>
        /// Gets or sets the weight
        /// </summary>
        public decimal Weight { get; set; }

        /// <summary>
        /// Gets or sets the length
        /// </summary>
        public decimal Length { get; set; }

        /// <summary>
        /// Gets or sets the width
        /// </summary>
        public decimal Width { get; set; }

        /// <summary>
        /// Gets or sets the Height
        /// </summary>
        public decimal Height { get; set; }
    }
}
