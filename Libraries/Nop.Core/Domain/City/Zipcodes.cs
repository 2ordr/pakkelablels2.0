﻿using System;
using System.Collections.Generic;

namespace Nop.Core.Domain.City
{
    public partial class Zipcodes : BaseEntity
    {
        public string Zipcode { get; set; }

        public string City { get; set; }
    }
}
