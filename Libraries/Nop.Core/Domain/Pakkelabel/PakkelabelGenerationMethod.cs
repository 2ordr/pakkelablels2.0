﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Nop.Core.Domain.Pakkelabel
{
    /// <summary>
    /// Represents a PakkeLabel
    /// </summary>
    public partial class PakkelabelGenerationMethod : BaseEntity
    {

        //public PakkelabelGenerationMethod()
        //{
        //    SupportedMethod = new Dictionary<string, string> 
        //    {
        //        { "TEST","true" },
        //        { "PROD", "false"}
        //    };
        //}

        public string Setting { get; set; }

        public string Value { get; set; }

        //public Dictionary<string, string> SupportedMethod { get; set; }
    }
}
