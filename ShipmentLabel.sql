
CREATE TABLE [dbo].[ShipmentLabel](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[OrderId] [int]  NULL,
	[ShippingID] [varchar] (1000)  NULL,
	[PackageNo] [varchar] (1000)  NULL,
	[AdminComment] [varchar] (1000)  NULL,
	[LabelOrderId] [varchar] (500)  NULL,
	[TrackingNumber] [varchar] (500)  NULL,
	[CreationDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO